﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Launcher
{
    public partial class UpdateForm : Form
    {
        public static string status = "Checking for updates...";
        public static int progress = 0;
        public UpdateForm()
        {
            InitializeComponent();
            this.Show();
            label1.Text = status;
            Thread update;
            update = new Thread(UpdateNow.go);
            update.Start();
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = status;
            if (label1.Text == "Updating...")
            {
                progressBar1.Value = progress;
                progressBar1.Style = ProgressBarStyle.Continuous;
            }
        }
    }
}
