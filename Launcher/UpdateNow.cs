﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Launcher
{
    class UpdateNow
    {
        public static void go()
        {
            try
            {
                string b = "";
                try
                {
                    b = File.ReadAllLines("verinfo")[0];
                }
                catch
                {
                }
                string a = "http://updates.koyu.space/yggdrasil/latest.txt";
                if (File.ReadAllText("updateserver") == "lts")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_lts.txt";
                }
                if (File.ReadAllText("updateserver") == "dev")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_dev.txt";
                }
                if (File.ReadAllText("updateserver") == "beta")
                {
                    a = "http://updates.koyu.space/yggdrasil/latest_beta.txt";
                }
                a = new WebClient().DownloadString(a).Split('\n')[0];
                if (a != b)
                {
                    UpdateForm.status = "Updating...";
                    if (File.Exists("Yggdrasil.exe"))
                    {
                        File.Delete("Yggdrasil.exe");
                    }
                    WebClient w = new WebClient();
                    w.DownloadProgressChanged += (sss, eee) =>
                    {
                        UpdateForm.progress = eee.ProgressPercentage;
                    };
                    w.DownloadFileCompleted += (s, e) =>
                    {
                        UpdateForm.progress = 0;
                        if (!Directory.Exists("de/"))
                        {
                            Directory.CreateDirectory("de/");
                        }
                        WebClient w2 = new WebClient();
                        w.DownloadProgressChanged += (ssss, eeee) =>
                        {
                            UpdateForm.progress = eeee.ProgressPercentage;
                        };
                        w2.DownloadFileCompleted += (ss, ee) =>
                        {
                            Process.Start("Yggdrasil.exe");
                            Environment.Exit(0);
                        };
                        w2.DownloadFileAsync(new Uri("https://updates.koyu.space/yggdrasil/Yggdrasil.resources.dll"), "de/Yggdrasil.resources.dll");
                    };
                    w.DownloadFileAsync(new Uri("http://updates.koyu.space/yggdrasil/Yggdrasil.exe"), "Yggdrasil.exe");
                }
                else
                {
                    Process.Start("Yggdrasil.exe");
                    Environment.Exit(0);
                }
            }
            catch
            {
                try
                {
                    if (File.Exists("Yggdrasil.exe"))
                    {
                        Process.Start("Yggdrasil.exe");
                        Environment.Exit(0);
                    }
                    else
                    {
                        Form1 f = new Form1();
                        f.Show();
                    }
                } catch { }
            }
        }
    }
}
